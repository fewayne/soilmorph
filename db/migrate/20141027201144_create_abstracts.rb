class CreateAbstracts < ActiveRecord::Migration
  def change
    create_table :abstracts do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :organization
      t.string :country
      t.string :other_authors
      t.string :title
      t.string :abstract
      t.timestamps
    end
  end
end
