json.extract! @abstract, :id, :first_name, :last_name, :email, :organization, :country, :other_authors, :title, :abstract, :created_at, :updated_at
