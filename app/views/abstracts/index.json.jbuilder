json.array!(@abstracts) do |abstract|
  json.extract! abstract, :id, :first_name, :last_name, :email, :organization, :country, :other_authors, :title, :abstract
  json.url abstract_url(abstract, format: :json)
end
