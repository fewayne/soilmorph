class AbstractMailer < ActionMailer::Base
  default from: "fewayne@wisc.edu"
  def submitted(abstract)
    @abstract = abstract

    mail to: "hartemink@wisc.edu", cc: "fewayne@wisc.edu"
  end
  
end
