class AbstractsController < ApplicationController
  before_action :set_abstract, only: [:show, :edit, :update, :destroy]
  before_filter :trim_abstract
  
  def trim_abstract
    if params[:abstract]
      if params[:abstract][:abstract]
        params[:abstract][:abstract] = (params[:abstract][:abstract])[0..Abstract::ABSTRACT_SIZE]
      end
    end
  end
        
  # GET /abstracts
  # GET /abstracts.json
  def index
    @abstracts = Abstract.all.sort { |a, b| a.created_at <=> b.created_at }
    logger.info "found #{@abstracts.size} abstracts"

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @abstracts }
    end
  end

  # GET /abstracts/1
  # GET /abstracts/1.json
  def show
  end

  # GET /abstracts/new
  def new
    @abstract = Abstract.new
  end

  # GET /abstracts/1/edit
  def edit
  end

  # POST /abstracts
  # POST /abstracts.json
  def create
    logger.info("Create: #{abstract_params.inspect}")
    @abstract = Abstract.new(abstract_params)
    logger.info("After new: #{@abstract.inspect}")

    respond_to do |format|
      if @abstract.save
        mail = AbstractMailer.submitted(@abstract)
        mail.deliver
        
        format.html { redirect_to @abstract, notice: 'Abstract was successfully created.' }
        format.json { render :show, status: :created, location: @abstract }
      else
        format.html { render :new }
        format.json { render json: @abstract.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /abstracts/1
  # PATCH/PUT /abstracts/1.json
  def update
    respond_to do |format|
      if @abstract.update(abstract_params)
        format.html { redirect_to @abstract, notice: 'Abstract was successfully updated.' }
        format.json { render :show, status: :ok, location: @abstract }
      else
        format.html { render :edit }
        format.json { render json: @abstract.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /abstracts/1
  # # DELETE /abstracts/1.json
  # def destroy
  #   @abstract.destroy
  #   respond_to do |format|
  #     format.html { redirect_to abstracts_url, notice: 'Abstract was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_abstract
      @abstract = Abstract.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def abstract_params
      params.require(:abstract).permit(:first_name, :last_name, :email, :organization, :country, :other_authors, :title, :abstract)
    end
end
