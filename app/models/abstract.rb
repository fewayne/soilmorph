class Abstract < ActiveRecord::Base
  ABSTRACT_SIZE=3500
  validates :last_name, :presence => true
  validates :title, :presence => true
  validates :organization, :presence => true
  validates :abstract, length: {within: 10..ABSTRACT_SIZE}
  validates :email, :format => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/
end
